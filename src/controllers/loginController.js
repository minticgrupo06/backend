const login = require('../models/login');

class LoginController {

    constructor() {

    }

    //registrar
    registrar(req, res) {
        login.create(req.body, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(201).json(data);
            }
        });
    }

    //consultar
    getLogin(req, res) {
        login.find((error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }

    //actualizar
    setLogin(req, res) {
        let { id, nombre, apellido, email, usuario, password } = req.body;
        let obj = {
            nombre, apellido, email, usuario, password
        };
        login.findByIdAndUpdate(id, { $set: obj }, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }

    //eliminar
    delete(req, res) {
        let { id } = req.body;
        login.findByIdAndRemove(id, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }




}

module.exports = LoginController;
