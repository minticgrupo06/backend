const materiales = require('../models/materiales');

class MaterialesController {

    constructor() {

    }

    //registrar
    registrar(req, res) {
        materiales.create(req.body, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(201).json(data);
            }
        });
    }

    //consultar
    getMateriales(req, res) {
        materiales.find((error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }

    //actualizar
    setMateriales(req, res) {
        let { id_proveedor,tipo_material,cantidad } = req.body;
        let obj = {
            id_proveedor,tipo_material,cantidad
        };
        materiales.findByIdAndUpdate(id, { $set: obj }, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }

    //eliminar
    delete(req, res) {
        let { id } = req.body;
        materiales.findByIdAndRemove(id, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }




}

module.exports = MaterialesController;