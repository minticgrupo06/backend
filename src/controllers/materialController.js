const Material = require('../models/material');
const Proveedor = require('../models/proveedor');

class MaterialController {

    constructor() {

    }

    //registrar
    registrar(req, res) {
        Material.create(req.body, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(201).json(data);
            }
        });
    }

    //consultar
    getMaterial(req, res) {
        Material.
            find({}).
            populate("proveedor").
            exec((error,data) => {  
                if (error) {
                    res.status(500).json({ error });
                } else {
                    res.status(200).json(data);
                }
            })
    }
  //actualizar 
    setMaterial(req,res){      
    let { id, tipo_material, cantidad, proveedor } = req.body;
    let obj = {
        tipo_material, cantidad, proveedor
    };
    Material.findByIdAndUpdate(id, { $set: obj }, (error, data) => {
        if (error) {
            res.status(500).json({ error });
        } else {
            res.status(200).json(data);
        }
    });
}
    
    
        
    



    //eliminar
    delete(req, res) {
        let { id } = req.body;
        Material.findByIdAndRemove(id, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }




}

module.exports = MaterialController;
