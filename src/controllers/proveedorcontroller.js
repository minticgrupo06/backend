const proveedores = require('../models/proveedores');

class ProveedoresController {

    constructor() {

    }

    //registrar
    registrar(req, res) {
        proveedores.create(req.body, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(201).json(data);
            }
        });
    }

    //consultar
    getProveedor(req, res) {
        proveedores.find((error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }

    //actualizar
    setProveedor(req, res) {
        let { tipo_identificacion, numero_identificacion, tipo_persona, direccion, ciudad, telefono, email, contacto, tipo_material } = req.body;
        let obj = {
            tipo_identificacion, numero_identificacion, tipo_persona, direccion, ciudad, telefono, email, contacto, tipo_material
        };
        proveedores.findByIdAndUpdate(id, { $set: obj }, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }

    //eliminar
    delete(req, res) {
        let { id } = req.body;
        proveedores.findByIdAndRemove(id, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }




}

module.exports = ProveedoresController;