const proveedor = require('../models/proveedor');



class UserController {

    constructor() {

    }
    //Obtener todos los usuarios
    getAllUsers(req, res) {
        proveedor.find((error, data) =>{
            if(error){
                res.status(500).send();
            }else{
                res.status(200).json(data);
            }
        })
    }


    getUserById(req, res) {
        let id = req.params.id;
        proveedor.findById(id, (error, data)=>{
            if(error){
                res.status(500).send();
            }else{
                res.status(200).json(data);
            }
        });
    }
    

    register(req, res){
        proveedor.create(req.body, (error, data)=>{
            if(error){
                res.status(500).json(error);
            }else{
                res.status(201).json(data);
            }
        });
    }


    deleteUser(req, res){
        let { id } = req.body;
        proveedor.findByIdAndRemove(id, (error, data)=>{
            if(error){
                res.status(500).send();
            }else{
                res.status(200).json(data);
            }
        });
    }

    update(req, res){
        let { id, tipo_identificacion, numero_identificacion, tipo_persona, razon_social, direccion, ciudad, telefono, email, contacto } = req.body;
        let objProveedor = {
            tipo_identificacion,
            numero_identificacion,
            tipo_persona,
            razon_social,
            direccion,
            ciudad,
            telefono,
            email,
            contacto
            
            
        };
        proveedor.findByIdAndUpdate(id, {
            $set: objProveedor
        }, (error, data)=>{
            if(error){
                res.status(500).send();
            }else{
                res.status(200).json(data);
            }
        });
    }



}

exports.default = UserController;
