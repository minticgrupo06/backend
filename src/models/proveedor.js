const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const proveedorSchema = new Schema({
    tipo_identificacion: {
        type: String
    },
    numero_identificacion: {
        type: Number
    },
    tipo_persona: {
        type: String
    },
    razon_social: {
        type: String
    },
    direccion: {
        type: String
    },
    ciudad: {
        type: String
    },
    telefono: {
        type: Number
    },
    email: {
        type: String
    },
    contacto: {
        type: String
    }
    
}, {
    collection: "proveedores"
});

module.exports = mongoose.model('Proveedor', proveedorSchema);
