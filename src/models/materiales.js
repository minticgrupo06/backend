const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const materialesSchema = new Schema({
    id_proveedor:{
        type:String
    },
    tipo_material:{
        type:String
    },
    cantidad:{
        type:Number
    }   
}, {
    collection: "materiales"
});

module.exports = mongoose.model('materiales', materialesSchema);
