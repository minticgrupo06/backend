const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const usuarioSchema = new Schema({

    usuario:{
        type:String
    },
    contraseña:{
        type:String
    }   
}, {
    collection: "usuarios"
});

module.exports = mongoose.model('Usuario', usuarioSchema);
