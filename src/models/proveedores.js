const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const proveedoresSchema = new Schema({
    tipo_identificacion:{
        type:String
    },
    numero_identificacion:{
        type:Number
    },    
    tipo_persona:{
        type:String
    },
    razon_social: {
        type: String
    },
    direccion: {
        type: String
    },
    ciudad:{
        type:String
    },
    email: {
        type: String
    },
    telefono: {
        type: Number
    },
    contacto: {
        type: String
    },
    tipo_material:{
        
    }
}, {
    collection: "proveedores"
});

module.exports = mongoose.model('proveedores', proveedoresSchema);
