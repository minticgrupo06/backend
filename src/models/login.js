const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const loginSchema = new Schema({
    nombre: {
        type: String
    },
    apellido: {
        type: String
    },
    email: {
        type: String
    },
    usuario: {
        type: String
    },
    password: {
        type: String
    }

}, {
    collection: 'the_login'
});

module.exports = mongoose.model('Login', loginSchema);