const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const materialSchema = new Schema({
    tipo_material: {
        type: String
    },
    cantidad: {
        type: Number
    },
    proveedor: {
        type: Schema.ObjectId, ref:"Proveedor"
    }
}, {
    collection: 'materiales'
});

module.exports = mongoose.model('Material', materialSchema);
