const express = require('express');
const LoginController = require('../controllers/loginController');

class LoginRouter {

    constructor() {
        this.router = express.Router();
        this.config();
    }

    config() {
        const objLoginC = new LoginController();
        this.router.post("/login", objLoginC.registrar);
        this.router.get("/login", objLoginC.getLogin);
        this.router.put("/login", objLoginC.setLogin);
        this.router.delete("/login", objLoginC.delete);
    }

}

module.exports = LoginRouter;
