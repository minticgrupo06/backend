const express = require('express');
const ProveedoresController = require('../controllers/proveedorController');

class ProveedoresRouter {

    constructor() {
        this.router = express.Router();
        this.config();
    }

    config() {
        const proveedores = new ProveedoresController();
        this.router.post("/proveedores",  proveedores.registrar);
        this.router.get("/proveedores",  proveedores.getProveedor);
        this.router.put("/proveedores", proveedores.setProveedor);
        this.router.delete("/proveedores", proveedores.delete);
    }

}

module.exports = ProveedoresRouter;
