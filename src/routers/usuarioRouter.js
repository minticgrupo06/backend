const express = require('express');
const usuarioController = require('../controllers/usuarioController');

class UsuarioRouter {

    constructor() {
        this.router = express.Router();
        this.config();
    }

    config() {
        const usuario = new usuarioController.default();
        this.router.post("/usuarios", usuario.registrar);
        this.router.get("/usuarios",  usuario.getUsuario);
        this.router.put("/usuarios", usuario.setUsuario);
        this.router.delete("/usuarios", usuario.delete);
    }

}

module.exports = UsuarioRouter;
