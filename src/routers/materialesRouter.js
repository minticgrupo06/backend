const express = require('express');
const materialesController = require('../controllers/materialesController');
const materiales = require('../models/materiales');

class MaterialesRouter {

    constructor() {
        this.router = express.Router();
        this.config();
    }

    config() {
        const materiales = new materialesController();
        this.router.post("/materiales",  materiales.registrar);
        this.router.get("/materiales",   materiales.getMateriales);
        this.router.put("/materiales", materiales.setMateriales);
        this.router.delete("/materiales", materiales.delete);
    }

}

module.exports = MaterialesRouter;
