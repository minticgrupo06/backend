const express = require('express');
const MaterialController = require('../controllers/materialController');

class MaterialRouter {

    constructor() {
        this.router = express.Router();
        this.config();
    }

    config() {
        const objMaterialC = new MaterialController();
        this.router.post("/material", objMaterialC.registrar);
        this.router.get("/material", objMaterialC.getMaterial);
        this.router.put("/material", objMaterialC.setMaterial);
        this.router.delete("/material", objMaterialC.delete);
    }

}

module.exports = MaterialRouter;
